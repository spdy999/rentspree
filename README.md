# Rentspree test

## Run project

- clone all submodules
  - `git clone --recurse-submodules git@gitlab.com:spdy999/rentspree.git`
- setup `rentspree-server`
  - `cd rentspree-server`
  - config mongodb env in `nodemon.json`

  ```text
    {
        "env": {
        "MONGO_USER": "admin",
        "MONGO_PASSWORD": "admin123",
        "MONGO_DB": "rentspree",
        "MONGO_PORT": 27017
        }
    }
    ```

  - install project
    - `yarn`
  - start server
    - `yarn start`

- setup `rentspree-client`
  - `cd rentspree-client`
    - install project
      - `yarn`
    - start client
      - `yarn start`

## Run test rentspree-client

- `cd rentspree-client`
- `yarn test`

---
**NOTE**

if you have any problems, feel free to contact me at +66917689666


---
## Example

- When project started.
![Imgur](https://imgur.com/4S46zs0.png)

- test add 1st todo
![Imgur](https://imgur.com/ehekGjt.png)

- test click `COMPLETE` todo
![Imgur](https://imgur.com/SFQimaM.png)

- test delete 2nd todo
![Imgur](https://imgur.com/GC2kgZf.png)
![Imgur](https://imgur.com/Jta3zNe.png)